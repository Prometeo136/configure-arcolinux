#! /bin/bash
LIST_OF_APPS="ansible"

# Update system
echo "Updating system ........."
sudo pacman --sync --refresh --sysupgrade --noconfirm

# Install Ansible
echo "Installing ansible ......"
sudo pacman -S ansible --noconfirm

echo "Running ansible playbook ......."
ansible-playbook -K configure.yaml

# oh-my-zsh
echo "Installing oh-my-zsh ..."
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "Installing antibody .."
curl -sL git.io/antibody | sh -s
# installing powerline zsh
echo "Installing powerline with pip ..."
pip install powerline-status --user
echo "Setting zsh as default shell ..."
chsh -s /bin/zsh
